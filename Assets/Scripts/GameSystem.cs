﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameSystem : stateMachine
{
    /**<summary> Game system where children from State Machine  class to Control states and Player Command
     * <EXECUTION> start, for setup all characters,
     *  Update for On click mouse Commands 
     * </EXECUTION>
     * 
     * <state> 
     *  controlling player state turn on prepare and execution
     * </state>
     * 
     * <deepInsideMethod>
     * all methods that manage characters data structure
     * UI Manager
     * On Click command 
     * </deepInsideMethod>
     * 
     * </summary>
     * 
     */

    #region Properties
    [Header("Character")]
    public List<character> characters;
    public character selectedChar { get; set; }
    public character selectedCharEnemy { get; private set; }
    public List<enemyObject> activeEnemies { get; private set; }
    public List<playerObject> activePlayers { get; private set; }

    [Header("raycast")]
    public LayerMask contactLayer;
    private Camera mainCam;

    [Header("Tile Environment")]
    public tileManager _tileManager;
    public floorTile target { get; set; }

    [Header("Text Indicator")]
    [SerializeField] private TextMeshProUGUI turn_Text;
    [SerializeField] private TextMeshProUGUI stateIndicator_Text;
 
    [Header("Player State")]
    public playerState _playerState;

    [Header("UI")]
    public Transform ButtonsGroup;
    [SerializeField]private GameObject OverlayEnd;
    [SerializeField]private TextMeshProUGUI gameStatus;
    #endregion

    #region E X E C U T I O N
    private void Awake()
    {
        Application.targetFrameRate = 60;
    }

    private void Start()
    {
        mainCam = Camera.main;
        setupCharactersOnStart();
        
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && _playerState != playerState.bussy)
        {

            StartCoroutine( onClickCommand());

        }
    }
    #endregion

    #region Game State
    public void onPrepareAttackButton()
    {
        if (selectedChar != null)
        {
            _playerState = playerState.attack;
            StartCoroutine(State.prepareAttack());

        }
        else
        {
            setStateText("Select Player Character");
        }
        
    }

    public void onPrepareMoveButton()
    {

        if (selectedChar != null)
        {
            _playerState = playerState.move;
            StartCoroutine(State.prepareMove());
        }
        else
        {
            setStateText("Select Player Character");
        }
        
        // _tileManager.clearTiles();
        // _playerState = playerState.move;
    }  
   
    public void onEndTurnButton()
    {
        if (_playerState != playerState.bussy)
        {
            _tileManager.clearTiles();
            if (selectedChar != null)
            {
                selectedChar.deselect();
                selectedChar = null;
            }

            setState(cacheMemory.cache._enemyTurn);
        }

    }

    public void onRestartButton()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void onExitButton()
    {
        Application.Quit();
    }

    public void onAttack()
    {
        StartCoroutine(State.Attack());
    }
    public void onMove()
    {
        StartCoroutine(State.Move(1f));
    }
    #endregion

    #region Deep Inside Methods
    public void setupCharactersOnStart()
    {
        activeEnemies = new List<enemyObject>();
        activePlayers = new List<playerObject>();
        foreach (var character in characters)
        {
            if (character.GetType() == typeof(enemyObject))
            {
                character.setupAtStart(_tileManager,insideTile.enemy);
                activeEnemies.Add((enemyObject)character);
            }
            else
            {
                character.setupAtStart(_tileManager, insideTile.player);
                activePlayers.Add((playerObject)character);
            }
        }
        setState(cacheMemory.cache._enemyTurn);
    }

    public void setTurnIndicatorText(string _text)
    {
        turn_Text.text = _text;
    }

    public void setStateText(string _text)
    {
        stateIndicator_Text.text = _text;
    }

    private character searchCharInside(int id)
    {
        foreach (var character in characters)
        {
            if (character != null && character.tileId == id)
            {
                return character;
            }
        }
        return null;
    }

    public void removeCharacter(character chars)
    {
        characters.Remove(chars);

        if (chars.GetType() == typeof(enemyObject))
        {
           
            activeEnemies.Remove((enemyObject)chars);
        }
        else
        {
            
            activePlayers.Remove((playerObject)chars);
        }
    }

    private IEnumerator onClickCommand()
    {
        RaycastHit2D hit = Physics2D.Raycast(mainCam.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, contactLayer);

        if (hit.transform != null)
        {
            #region select Character
            if (hit.transform.CompareTag("player") && !hit.transform.GetComponent<character>().isDisabled)
            {
                _tileManager.clearTiles();
                yield return null;
                if (selectedChar != null)
                {
                    selectedChar.deselect();
                }

                selectedChar = hit.transform.GetComponent<playerObject>();
                selectedChar.selected();
                setStateText("Select Command Mode");
                //onPrepareMoveClick();
            }
            #endregion

            #region State Action
            if (hit.transform.CompareTag("tile") && _playerState == playerState.move)
            {
                target = hit.transform.GetComponent<floorTile>();

                if (target.isActive)
                {
                    onMove();
                }

            }

            if (hit.transform.CompareTag("tile") && _playerState == playerState.attack)
            {
                target = hit.transform.GetComponent<floorTile>();
                selectedCharEnemy = searchCharInside(target.id);
                if (target.isActive && target.inside == insideTile.enemy)
                {
                    onAttack();
                }

            }
            #endregion

        }
    }
   
    public void endGame(state _state)
    {
        OverlayEnd.SetActive(true);
        if (_state.GetType() == typeof(won))
        {
            gameStatus.text = "You Win!";
        }
        else
        {
            gameStatus.text = "You Lose!";
        }
    }

    public void disableButton()
    {
        for (int i = 0; i < ButtonsGroup.childCount; i++)
        {
            ButtonsGroup.GetChild(i).GetComponent<Button>().interactable = false;
        }
        
    }
    public void enableButton()
    {
        for (int i = 0; i < ButtonsGroup.childCount; i++)
        {
            ButtonsGroup.GetChild(i).GetComponent<Button>().interactable = true;
        }

    }
    #endregion

}
