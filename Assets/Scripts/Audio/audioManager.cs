﻿using UnityEngine.Audio;
using UnityEngine;
using System;

public class audioManager : MonoBehaviour
{
    public static audioManager _audioMan;
    public audio[] sounds;
    // Start is called before the first frame update

    private void Awake()
    {
        if (_audioMan == null)
        {
            _audioMan = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
        addAudioSource();
    }
    
    void Start()
    {
        Play("BGM");
    }

    public void Play (string name)
    {
        audio sound = Array.Find(sounds, audio => audio.name == name);
        if (sounds == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
        }
        sound.source.Play();
    }

    public void Stop (string name)
    {
        audio sound = Array.Find(sounds, audio => audio.name == name);
        if (sounds == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
        }
        sound.source.Stop();
    }

    private void addAudioSource()
    {
        GameObject child = transform.GetChild(0).gameObject;
        foreach (var sound in sounds)
        {
            sound.source = child.AddComponent<AudioSource>();
            sound.source.clip = sound.clip;
            sound.source.volume = sound.volume;
            sound.source.pitch = sound.pitch;
            sound.source.loop = sound.loop;
        }
    }
}
