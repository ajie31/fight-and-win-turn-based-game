﻿
using UnityEngine;

[System.Serializable]
public class audio 
{
    [Header("Audio Name")]
    public string name;

    [Header("Audio Profile")]
    public AudioClip clip;
    public audioType _audioType;

    [Range(0f, 1f)]
    public float volume;
    [Range(.1f, 2f)]
    public float pitch;

    public bool loop;

    public AudioSource source { get; set; }
    
    public enum audioType
    {
        BGM,
        sfx
    }

}
