﻿
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public abstract class character :MonoBehaviour
{
    /** <summary> This is abstract class for Player and Enemy Parent
     * for handle variaty of Character unique Attack pattern, move pattern
     * , take damage/ reduce health, player state, and controll animation.
     * </summary>
     */
    #region editor properties
    [Header("Profile")]
    [SerializeField] private characterSO characterObj;
    public int tileId;
    public bool isDisabled;

    [Header("UI")]
    [SerializeField] private Image healthBar;
    [SerializeField] private TextMeshProUGUI nameText;
    [SerializeField] private SpriteRenderer renderer;
    private Color selectedColor = new Color(0, .8f, 0);
    #endregion

    #region Non Editor Properties
    public bool isBussy { get; set; }
    public Animator animator { get; private set; }
    public float damage { get; private set; }
    public float health { get; private set; }
    public int moveRange { get; private set; }
    public int attackRange { get; private set; }
    public int[] patternMove { get; private set; }
    public int[] patternAttack { get; private set; }
    public string name { get; private set; }
    private Color disableColor = new Color(0.6f, 0.6f, 0.6f);
    #endregion


    public bool takeDamage(float damage) 
    {
        
        health -= damage;
        textPop.text.getTextPop(transform.position,damage);
        healthBar.fillAmount = health / characterObj.health;
        return health <= 0;
        
    }

    protected void setupStats()
    {
        health = characterObj.health;
        damage = characterObj.damage;
        healthBar.fillAmount = health / characterObj.health;
        patternMove = characterObj.patternMove;
        patternAttack = characterObj.patternAttack;
        moveRange = characterObj.moveRange;  
        attackRange = characterObj.attackRange;
        nameText.text = characterObj.name;
        name = characterObj.name;
    }

    public void setupAtStart(tileManager tile,insideTile inside)
    {
        tile.CharacterStart(transform, tileId);
        animator = GetComponent<Animator>();
        tile.tiles[tileId].inside = inside;
    }

    #region virtual override
    public virtual void characterMove(GameSystem system, float duration = 1f)
    {

    }
    public virtual void characterAttack(GameSystem system, character chars)
    {

    }

    #endregion

    #region Animation Controll
    public void isdead()
    {
        gameObject.SetActive(false);
    }
    public void attackOnce()
    {
        animator.SetTrigger("idle");
    }

    #endregion

    #region Pointer Reaction
    public void enable()
    {
        isDisabled = false;
        renderer.color = Color.white;
    }
    public void disable()
    {
        isDisabled = true;
        renderer.color = disableColor;
        deselect();
    }

    public void selected()
    {
        nameText.color = selectedColor;
    }
    public void deselect()
    {
        nameText.color = Color.white;
    }


    #endregion

    #region Player Audio
    public void audioOnMove()
    {
        audioManager._audioMan.Play("move");
    }
    public void audioOnAttack()
    {
        audioManager._audioMan.Play("attack");
    }

    public void audioOnHurt()
    {
        audioManager._audioMan.Play("hurt");
    }

    public void audioOnDead()
    {
        audioManager._audioMan.Play("death");
    }
    #endregion

}
