﻿using System.Collections;
using UnityEngine;

public class enemyObject : character
{
    /** <summary> Children of Characters that override Character Move 
     * and character Attack </summary>
     */
    void Start()
    {
        setupStats();     
    }

    public override void characterMove(GameSystem system, float duration = 1f)
    {
        isBussy = true;
        StartCoroutine(moveCommand(system));
    }

    public override void characterAttack(GameSystem system, character chars)
    {
        isBussy = true;
        StartCoroutine(attackCommand(system, chars));
    }


    private IEnumerator moveCommand(GameSystem system)
    {
        Vector2 targetTile;
        
        #region Enemy Move

        system.setStateText("Choosing Path Move");
        yield return cacheMemory.cache.wait_1Sec;

        system._tileManager.tiles[tileId].inside = insideTile.empty;
        system._tileManager.setupEnemyMove(tileId, patternMove, moveRange);
        yield return cacheMemory.cache.wait_1Sec;

        system.target = system._tileManager.playerNearMove(system.activePlayers, transform.position);
        targetTile = system.target.transform.position;
        yield return null;

        if (targetTile.x - transform.position.x > 0)
        {
            GetComponent<SpriteRenderer>().flipX = true;
        }
        else
        {
            GetComponent<SpriteRenderer>().flipX = false;
        }

        system.setStateText("Moving");
        animator.SetTrigger("move");
        system._tileManager.clearTiles();

        float percent = 0, timeElapsed = 0, duration = 1f;

        while (true)
        {
            percent = timeElapsed / duration;

            transform.position = Vector2.Lerp(transform.position, targetTile, percent);

            timeElapsed += Time.deltaTime;

            if (percent >= 1)
            {
                tileId = system.target.id;
                animator.SetTrigger("idle");
                system._tileManager.tiles[tileId].inside = insideTile.enemy;
                break;
            }
            yield return null;

        }

        #endregion

        #region end of State
        animator.SetTrigger("idle");
        disable();
        isBussy = false;
        #endregion

    }

    private IEnumerator attackCommand(GameSystem system,character player)
    {
        //playerObject player;
        #region Attack!
        animator.SetTrigger("attack");
        bool isPlayerDead = player.takeDamage(damage);
        player.animator.SetTrigger("hurt");

        if (isPlayerDead)
         {
            player.animator.SetTrigger("dead");
            system._tileManager.tiles[player.tileId].inside = insideTile.empty;
            system.removeCharacter(player);

            if (system.activePlayers.Count <= 0)
            {
                StopAllCoroutines();
                system.setState(cacheMemory.cache.hasLose);
            }
        }
        system._tileManager.clearTiles();
        yield return cacheMemory.cache.wait_1Sec;

        // continue;
        #endregion

        #region end of State
       
        disable();
        isBussy = false;
        #endregion
    }
}
