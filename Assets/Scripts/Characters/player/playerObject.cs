﻿using System.Collections;
using UnityEngine;

public class playerObject : character
{

    /** <summary> Children of Characters that override Character Move 
     * and character Attack </summary>
     */
    private void Start()
    {
        setupStats();
    }

    public override void characterMove(GameSystem system, float duration = 1f)
    {
        StartCoroutine(moveCommand( system, duration));
    }

    public override void characterAttack(GameSystem system, character chars)
    {
        StartCoroutine(attackCommand(system, chars));
    }

    #region private methods
    private IEnumerator moveCommand(GameSystem system, float duration = 1f)
    {

        float timeElapsed = Time.deltaTime, percent = 0;
        Vector2 start = transform.position;
        Vector2 target = system.target.transform.position;
        if (target.x - start.x > 0)
        {
            GetComponent<SpriteRenderer>().flipX = true;
        }
        else
        {
            GetComponent<SpriteRenderer>().flipX = false;
        }
        setupMove(system);

        while (true)
        {
            percent = timeElapsed / duration;

            transform.position = Vector2.Lerp(start, target, percent);

            timeElapsed += Time.deltaTime;

            if (percent >= 1)
            {
                system._playerState = playerState.move;
                system.setStateText(name + " turns end");
                animator.SetTrigger("idle");
                break;
            }
            yield return null;

        }

        disable();
        system.selectedChar = null;
    }

    private IEnumerator attackCommand(GameSystem system, character enemy)
    {
        animator.SetTrigger("attack");
        system.setStateText("Attacking");
        yield return cacheMemory.cache.wait_halfSec;
        
        bool isDead = enemy.takeDamage(damage);
        enemy.animator.SetTrigger("hurt");
        system._tileManager.clearTiles();
        if (isDead)
        {
            enemy.animator.SetTrigger("dead");
            system._tileManager.tiles[enemy.tileId].inside = insideTile.empty;
            system.removeCharacter(system.selectedCharEnemy);

            if (system.activeEnemies.Count <= 0)
            {
                system.setState(cacheMemory.cache.hasWon);
            }
        }

        yield return cacheMemory.cache.wait_halfSec;
        disable();
        system.selectedChar = null;
    }

    private void setupMove(GameSystem system)
    {
        animator.SetTrigger("move");
        system._tileManager.tiles[tileId].inside = insideTile.empty;
        tileId = system.target.id;
        system._tileManager.clearTiles();
        system.target.inside = insideTile.player;
        system._playerState = playerState.bussy;
        system.setStateText("Moving");

    }
    #endregion

}
