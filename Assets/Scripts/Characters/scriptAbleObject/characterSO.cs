﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new character",menuName = "Character")]
public class characterSO : ScriptableObject
{
    /**<summary> all Character Profile As Scriptable Object </summary>
     * 
     */
    [Header("Character Status")]
    public string name;
    public float health;
    public float damage;
    
    [Header("Patterns Tile Action")]
    public int[] patternMove;
    public int[] patternAttack;
    [Header("Pattern Range")]
    public int moveRange;
    public int attackRange;
}
