﻿
using UnityEngine;

public abstract class stateMachine :MonoBehaviour
{
    /**<summary> stateMachine is a abstract class for Game system
     * 
     * state machine is controlling all state class
     * </summary>
     */
    protected state State;

    public void setState(state _State)
    {
        State = _State;
        StartCoroutine(State.Start());
    }
}
