﻿using System.Collections;
using UnityEngine;
public abstract class state
{
/**<summary> abstract Class for Every State Game 
 * include : player turn, enemy turn , won state, lose state
 * 
 * Gamesystem was called as constructor for pass it to child class
 * </summary>
 */

    protected GameSystem system;

    public state(GameSystem _system)
    {
        system = _system;
    }

    public virtual IEnumerator Start()
    {
        yield break;
    }

    public virtual IEnumerator Move(float time = 1f)
    {
        yield break;
    }

    public virtual IEnumerator Attack()
    {
        yield break;
    }

    public virtual IEnumerator prepareMove()
    {
        yield break;
    }
    
    public virtual IEnumerator prepareAttack()
    {
        yield break;
    }
}

public enum playerState
{
    move,
    attack,
    bussy
}
