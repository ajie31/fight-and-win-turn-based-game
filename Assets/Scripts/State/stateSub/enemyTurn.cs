﻿using System.Collections;

public class enemyTurn : state
{
    public enemyTurn(GameSystem _system) : base(_system)
    {
    }

    /**<summary>Children of State Class 
     * That only have One Command on Start mothods
     * because Enemy is A.I
     * </summary>
     * 
     * <inside_Start>
     * inside start method execute from enemyObject class Movement and Attack methods
     * and  tileManager preparation
     * </inside_Start>
     * 
     */

    public bool isStop { get; set; }
    public override IEnumerator Start()
    {
        #region setup
        system.disableButton();
        
        system._playerState = playerState.bussy;

        foreach (var character in system.characters)
        {
            if (character != null)
            {
                character.enable();
            }
            
        }
        #endregion

        #region start properties
        playerObject player;
        bool isAttacking;


        system.setTurnIndicatorText("Enemy Turns");
        system.setStateText("Idle");
        system.onPrepareMoveButton();
        yield return cacheMemory.cache.wait_1Sec; 
        #endregion

        foreach (var enemy in system.activeEnemies)
        {
            system.setStateText("Check If player Is Near");
            system._tileManager.setupAttackEnemy(enemy.tileId, enemy.patternAttack, enemy.attackRange);
            yield return cacheMemory.cache.wait_1Sec;
           
            player = system._tileManager.playerInAttackRange(system.activePlayers);
            yield return null;

            isAttacking = player != null;
            if (isAttacking)
            {
                enemy.characterAttack(system, player);
                while (true)
                {
                    if (!enemy.isBussy)
                        break;
                    yield return null;
                }
                continue;
            }
            else
            {
                system._tileManager.clearTiles();

                enemy.characterMove(system);
                while (true)
                {
                    if (!enemy.isBussy)
                        break;
                    yield return null;
                }
            }
    
        }
        system.setState(cacheMemory.cache._playerTurn);
    }
}
