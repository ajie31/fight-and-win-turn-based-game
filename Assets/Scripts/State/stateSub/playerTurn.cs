﻿
using System.Collections;

public class playerTurn : state 
{
    public playerTurn(GameSystem _system) : base(_system)
    {
    }

    public override IEnumerator Start()
    {
        yield return null;
        system.enableButton();

        foreach (var player in system.characters)
        {
            if (player != null)
            {
                player.enable();
            }
            player.enable();
        } 

        system.setTurnIndicatorText("Player Turns");
        system.setStateText("Select your Character");

        system._playerState = playerState.move;

        yield return null;
    }

    public override IEnumerator Move(float duration = 1f)
    {
        system.selectedChar.characterMove(system, duration);
        yield return null;
    }

    public override IEnumerator Attack()
    {
        system.selectedChar.characterAttack(system, system.selectedCharEnemy);
        yield return null;
        // system.setStateText("T");
    }

    public override IEnumerator prepareMove()
    {
        system.setStateText("Choose Path");
        system._tileManager.clearTiles();
        yield return null;
        system._tileManager.setupPlayerMove(system.selectedChar.tileId, system.selectedChar.patternMove, system.selectedChar.moveRange);
    }

    public override IEnumerator prepareAttack()
    {
        system. setStateText("Choose Enemy");
        system._tileManager.clearTiles();
        yield return null;
        system._tileManager.setupAttack(system.selectedChar.tileId, system.selectedChar.patternAttack, system.selectedChar.attackRange);
    }
   
    #region private player Turn

 

    #endregion

}
