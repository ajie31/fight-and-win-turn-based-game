﻿using System.Collections;

public class won : state
{
    /**<summary> when Enemy character list in game system is empty</summary>

    */
    public won(GameSystem _system) : base(_system)
    {
    }

    public override IEnumerator Start()
    {
        system.endGame(this);
        yield return null;
    }

}
