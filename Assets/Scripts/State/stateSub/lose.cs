﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lose : state
{
    /**<summary> when player character list in game system is empty</summary>

     */


    public lose(GameSystem _system) : base(_system)
    {
    }

    public override IEnumerator Start()
    {
        system.endGame(this);
        yield return null;
    }
}
