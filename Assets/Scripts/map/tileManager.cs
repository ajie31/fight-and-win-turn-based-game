﻿using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;

public class tileManager : MonoBehaviour
{
    /**<summary> Tile Manager is data structure of Tile Map Environment
     * manage player Positioning and movement
     * manage enemy Positioning and Movement
     * and detect surrounding for every Characters
     * </summary>
     */
     [Header("Floor Tile List")]
    public floorTile[] tiles;

    private List<floorTile> activeTile = new List<floorTile>();

    #region Tile management General

    public void CharacterStart(Transform obj,int id) 
    {
        obj.position = tiles[id].transform.position;
    }

    public void clearTiles()
    {
        if (activeTile.Count > 0)
        {
            foreach (var tile in activeTile)
            {
                tile.setColorEmppty();
                tile.isActive = false;
            }

            activeTile.Clear();
        }

    }

    public void setupTileId()
    {
        if (tiles.Length > 0)
        {
            for (int i = 0; i < tiles.Length; i++)
            {
                tiles[i].id = i;
            }
        }

    }

    #endregion

    #region Tile Management Player
    public void setupPlayerMove(int tile_Id, int[] pattern, int range)
    {

        int index = 0;
        for (int i = 1; i <= range; i++)
        {

            for (int j = 0; j < pattern.Length; j++)
            {
                index = tile_Id + pattern[j] * i;
                if (((tile_Id % 9 < i || tile_Id % 9 > i) && tile_Id % 9 < range && j == 2)
                    || ((8 - tile_Id % 9 < i || 8 - tile_Id % 9 > i)
                    && (8 - tile_Id % 9 >= 0 && 8 - tile_Id % 9 < range) && j == 0))
                {
                    
                    continue;
                }

                if (index >= 0 && index < tiles.Length)
                {

                    if (tiles[index].inside == insideTile.empty)
                    {
                        tiles[index].setColorPath();
                        tiles[index].isActive = true;
                        activeTile.Add(tiles[index]);
                    }
                    else if (tiles[index].inside == insideTile.enemy)
                    {
                        tiles[index].setColorEnemy();
                       
                        activeTile.Add(tiles[index]);
                    }
                }

            }
        }
    }

    public void setupAttack(int tile_Id, int[] pattern, int range)
    {
        int index = 0;
        for (int i = 1; i <= range; i++)
        {

            for (int j = 0; j < pattern.Length; j++)
            {
                index = tile_Id + pattern[j];
                if (((tile_Id % 9 < i || tile_Id % 9 > i) && tile_Id % 9 < range && (j == 4 || j == 5 || j == 3))
                   || ((8 - tile_Id % 9 < i || 8 - tile_Id % 9 > i)
                   && (8 - tile_Id % 9 >= 0 && 8 - tile_Id % 9 < range) && (j == 0 || j == 1 || j == 7)))
                {

                    continue;
                }
                if (index > 0 && index < tiles.Length)
                {
                    if (tiles[index].inside == insideTile.empty)
                    {

                        tiles[index].setColorPath();
                        tiles[index].isActive = true;
                        activeTile.Add(tiles[index]);
                    }
                    else if (tiles[index].inside == insideTile.enemy)
                    {
                        tiles[index].setColorEnemy();
                        tiles[index].isActive = true;
                        activeTile.Add(tiles[index]);
                    }
                }

            }
        }
    }
    #endregion

    #region Tile Management Enemy
    public void setupEnemyMove(int tile_Id, int[] pattern, int range)
    {
        int index = 0;
        for (int i = 1; i <= range; i++)
        {
           // Debug.Log("here");
            for (int j = 0; j < pattern.Length; j++)
            {
                
                index = tile_Id + pattern[j];
                if (((tile_Id % 9 < i || tile_Id % 9 > i) && tile_Id % 9 < range && j == 2)
                    || ((8 - tile_Id % 9 < i || 8 - tile_Id % 9 > i)
                    && (8 - tile_Id % 9 >= 0 && 8 - tile_Id % 9 < range) && j == 0))
                {
                    
                    continue;
                }

                if (index > 0 && index < tiles.Length - 1)
                {
                    if (tiles[index].inside == insideTile.empty)
                    {
                        tiles[index].setColorPath();
                        tiles[index].isActive = true;
                        activeTile.Add(tiles[index]);
                    }
                    else if (tiles[index].inside == insideTile.player)
                    {
                        tiles[index].setColorEnemy();
                        
                        activeTile.Add(tiles[index]);
                    }
                }

            }
        }
    }

    public void setupAttackEnemy(int tile_Id, int[] pattern, int range)
    {
        int index = 0;
        for (int i = 1; i <= range; i++)
        {

            for (int j = 0; j < pattern.Length; j++)
            {
                index = tile_Id + pattern[j];

                if (((tile_Id % 9 < i || tile_Id % 9 > i) && tile_Id % 9 < range && (j == 4 || j == 5 || j == 3))
                  || ((8 - tile_Id % 9 < i || 8 - tile_Id % 9 > i)
                  && (8 - tile_Id % 9 >= 0 && 8 - tile_Id % 9 < range) && (j == 0 || j == 1 || j == 7)))
                {

                    continue;
                }

                if (index > 0 && index < tiles.Length)
                {
                    if (tiles[index].inside == insideTile.empty)
                    {
                        tiles[index].setColorPath();
                        tiles[index].isActive = true;
                        activeTile.Add(tiles[index]);
                    }
                    else if (tiles[index].inside == insideTile.player)
                    {
                        tiles[index].setColorEnemy();
                        tiles[index].isActive = true;
                        activeTile.Add(tiles[index]);
                    }
                }

            }
        }
    }
   
    public floorTile playerNearMove(List<playerObject> activePlayer, Vector2 enemy)
    {
        
        Vector2 player = playerNear(activePlayer, enemy).transform.position;
        floorTile selectedTile = null;
        foreach (var tile in activeTile)
        {
            if (selectedTile == null)
            {
                selectedTile = tile;
            }
            else
            {
                if (Vector2.Distance(tile.transform.position,player) 
                    < Vector2.Distance(selectedTile.transform.position,player))
                {
                    selectedTile = tile;
                }
            }
        }
        return selectedTile;
    }
   
    private floorTile playerNear(List<playerObject> activePlayer,Vector2 enemy)
    {
        floorTile targetTile = null;

        foreach (var player in activePlayer)
        {
            if (targetTile == null)
            {
                targetTile = tiles[ player.tileId];
            }
            else
            {
                if (Vector2.Distance(enemy,player.transform.position)
                    < Vector2.Distance(enemy, targetTile.transform.position) 
                    )
                {
                    targetTile = tiles[player.tileId];
                }
            }
        }
        
        return targetTile;
    }
   
    public playerObject playerInAttackRange(List<playerObject> activePlayer)
    {
        foreach (var tile in activeTile)
        {
            if (tile.inside == insideTile.player)
            {
                foreach (var player in activePlayer)
                {
                    if (player.tileId == tile.id)
                    {
                        return player;
                    }
                }
            }
        }
        return null;
    }
    #endregion

}


#if UNITY_EDITOR
[CustomEditor(typeof(tileManager)), CanEditMultipleObjects]
public class tileEditor : Editor
{

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var style = new GUIStyle(GUI.skin.label) { alignment = TextAnchor.MiddleCenter, fontStyle = FontStyle.Bold };
        tileManager tileClass = (tileManager)target;


        EditorGUILayout.LabelField("Set Tiles ID", style);

        if (GUILayout.Button("Set Tiles ID"))
        {
            tileClass.setupTileId();

        }

    }


}
#endif