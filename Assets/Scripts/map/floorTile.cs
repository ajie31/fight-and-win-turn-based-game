﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class floorTile : MonoBehaviour
{
    /**<summary> every Tile data and information
     * id in floor tile is key for characters positions
     * inside in tile floor is a key for character pointer
     * </summary>
     */
    
    [Header("Tile Profile")]
    public int id;
    public insideTile inside;
    public bool isActive { get; set; }

    Color path = new Color(0,1,0,0.5f);
    Color onTarget = new Color(1, 0, 0, 0.5f);

    public void setColorPath()
    {
        GetComponent<SpriteRenderer>().color = path;
    }
    public void setColorEnemy()
    {
        GetComponent<SpriteRenderer>().color = onTarget;
    }
    public void setColorEmppty()
    {
        GetComponent<SpriteRenderer>().color = Color.white;
    }
}

public enum insideTile 
{ 
    empty,
    player,
    enemy
}

