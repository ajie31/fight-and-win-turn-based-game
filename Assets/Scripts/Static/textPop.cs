﻿using TMPro;
using UnityEngine;

/**<summary>
 * Text Pop up at Characters take Damage, for information damage input
 * </summary>
 * 
 */

public class textPop : MonoBehaviour
{
    public static textPop text;
    [SerializeField] private float duration;    
    [SerializeField] private TextMeshPro _text;

    private bool startFade;
    private float timeSinceStarted;
    private float startTimeLerp;
    private float percentage;

    private void Awake()
    {
        if (text == null)
        {
            text = this;
        }
    }
    public void getTextPop(Vector2 post,float value)
    {       
        _text.color = Color.yellow;
        _text.transform.position = post;
        _text.text = value.ToString();

        startTimeLerp = Time.time;
        timeSinceStarted = 0;
        percentage = 0;
        startFade = true;

        _text.gameObject.SetActive(true);
    }

    private void Update()
    {
        if (startFade)
        {
            timeSinceStarted = Time.time - startTimeLerp;
            percentage = timeSinceStarted / (duration);

            _text.alpha = Mathf.Lerp(1, 0, percentage);

            _text.transform.position += Vector3.up * Time.deltaTime;
       

            if (percentage >= 1f)
            {
                startFade = false;
                _text.gameObject.SetActive(false);
            }
        }


    }
}
