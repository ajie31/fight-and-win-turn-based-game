﻿
using UnityEngine;

public class cacheMemory : MonoBehaviour
{
    /**<summary>Singleton Class</summary>
     */

    public static cacheMemory cache;
    public GameSystem system;
    public WaitForSeconds wait_1Sec { get; private set; }
    public WaitForSeconds wait_halfSec { get; private set; }
  
    #region State Cache
    public playerTurn _playerTurn { get; private set; }
    public enemyTurn _enemyTurn { get; private set; }
    public won hasWon { get; private set; }
    public lose hasLose { get; private set; }
    #endregion

    private void Awake()
    {
        if (cache == null)
        {
            cache = this;
        }

        wait_1Sec = new WaitForSeconds(1f);
        wait_halfSec = new WaitForSeconds(.5f);
        _playerTurn = new playerTurn(system);
        _enemyTurn = new enemyTurn(system);
        hasWon = new won(system);
        hasLose = new lose(system);
    }
}
