﻿
using UnityEngine;
using UnityEngine.SceneManagement;

public class mainMenuManager : MonoBehaviour
{
    public GameObject tutorPage;
    public void playButton()
    {
        SceneManager.LoadScene(1);
    }

    public void howToPlayButton()
    {
        tutorPage.SetActive(true);
    }
    public void closeTutor()
    {
        tutorPage.SetActive(false);
    }

    public void exitButton()
    {
        Application.Quit();
    }
}
