﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;


public class pointerButton : MonoBehaviour, IPointerEnterHandler,IPointerExitHandler,IPointerClickHandler
{
    [Header("Button Event")]
    public UnityEvent _event; 

    private TextMeshProUGUI text;
    private Image background;
    private Color backgroundColor;
    private bool pointerEnter;
    

    private void Start()
    {
        text = transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        background = GetComponent<Image>();
        backgroundColor = background.color;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        _event.Invoke();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        text.color = Color.white;
        pointerEnter = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        text.color = Color.red;
        pointerEnter = false;
    }


    void Update()
    {
        if (pointerEnter && background.color.a < 1)
        {
            backgroundColor.a += Time.deltaTime * 10f;
            background.color = backgroundColor;
        }

        if (!pointerEnter && background.color.a > 0)
        {
            backgroundColor.a -= Time.deltaTime * 10f;
            background.color = backgroundColor;
        }
    }

}
